#include "menu.hpp"
#include "rwfunc.hpp"
#include <iomanip> //��� setw
#include <stdlib.h>
#include <windows.h>

using namespace std;

void Menu(ServiceStruct* Service,int &NC,bool &Open)
{
    int n=0;
    system("color A");
    while(1)
    {
        system("cls");
        cout << " 1. ������� ����" << endl;
        cout << " 2. ��������� ����" << endl;
        cout << " 3. ������� �������" << endl;
        cout << " 4. ������� �������" << endl;
        cout << " 5. ������������� ������" << endl;
        cout << " 6. ������� �� �����" << endl;
        cout << " 7. � ���������" << endl;
        cout << " 8. �����" << endl;
        cout << "\n\n�������� ����� ����: ";
        cin >> n;
        switch(n)
        {
            case 1: ReadFile(Service,NC,Open); break;
            case 2: WriteFile(Service,NC,Open); break;
            case 3: Add(Service,NC,Open); break;
            case 4: Del(Service,NC,Open); break;
            case 5: Edit(Service,NC,Open); break;
            case 6: PrintScreen(Service,NC,Open); break;
            case 7: FAbout(); break;
            case 8: exit(0); break;
            default: cerr << "\n������ ����� ���� �����������, ��������� ������������ �����!\n " << endl; break;
        }
        system("pause");
    }
}
void PrintScreen(ServiceStruct* Service,int &NC,bool &Open)
{
    if(!Open)
    {
        cerr << "������: ���� �� ������!\n\n��������� ��������� �������:\n"
                "1) ���� � ��������� ������� ������� � ���������� build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) ���� ����� ���������� .db � ������ Service.db" << endl;
        return;
    }
    system("cls");
    for(int i=0;i<NC;i++)
    {
          cout << setiosflags(ios::left);
          cout << setw(3)   << Service[i].Id;
          cout << setw(15)  << Service[i].StateNumber;
          cout << setw(25)  << Service[i].Name;
          cout << setw(10)  << Service[i].Data;
          cout << setw(8)   << Service[i].Time;
          cout << setw(40)  << Service[i].Type;
          cout << setw(10)  << Service[i].Cost << endl;
    }
    cout << "\n\n";
}
void FAbout()
{
    system("cls");
    cout << "��������� �������� �������� ������ ������������\n����� 2-4\n\n4 ������� (����������)\n���� ����������: 16.12.2020" << endl;
}
